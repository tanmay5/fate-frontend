import React, { Component } from "react";
import RunningBoard from "../RunningBoard/RunningBoard";
import "./Fateboard.css";
import { Route, Link } from "react-router-dom";
import FateDashboard from "../FateDashboard/FateDashboard";
import Projects from '../ProjectsList/Projects'
import VariableDashboard from '../FateDashboard/VariableDashboard'

export default class Fateboard extends Component {
  render() {
    return (
      <div className="fateboard-container">
        <header className="fateboard-header">
          <Link to="/fate/projects" className="fateboard-header-heading">
            Fluid-Demo
          </Link>
          {/* <div className="right-header">
            <Link to="/fate/running" className="running-button">
              Running
            </Link>
          </div> */}
        </header>
        <Route path="/fate/projects" component={Projects} exact />
        <Route path="/fate/running" component={RunningBoard} exact />
        <Route path="/fate/dashboard/:id" component={FateDashboard} exact />
        <Route path = "/fate/dashboards/:id" component = {VariableDashboard} />
      </div>
    );
  }
}

import React, { Component } from "react";
import "./FateDashboard.css";
import { CircularProgressbar } from "react-circular-progressbar";
import { connect } from "react-redux";
import {Link} from 'react-router-dom'


class FateDashboard extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    // console.log(this.props.match.params.id)
    return (
      <div className="dashboard-container">
        <div className="dashboard-header">
          <p>Dashboard</p>
          <p>Job: {this.props.match.params.id}</p>
          <Link to = "/fate/running" className = "back-button"><button>{"<<"}Back</button></Link>
        </div>
        <div className="dashboard-info-block">
          <div className="dataset-container">
            <p className = "dashboard-info-block-heading">DATASET INFO</p>
            <div className = "table">
              <p className = "table-head">th:</p>
              <p className = "table-body">td</p>
            </div>
            <div className = "table">
              <p className = "table-head">th:</p>
              <p className = "table-body">td</p>
            </div>
            <div className = "table">
              <p className = "table-head">th:</p>
              <p className = "table-body">td</p>
            </div>
            <div className = "table">
              <p className = "table-head">th:</p>
              <p className = "table-body">td</p>
            </div>
          </div>
          <div className="progress-container">
            <p className = "dashboard-info-block-heading">JOB</p>
            <div className = "progress-bar-container">
            <CircularProgressbar
                  value={this.props.jobs.filter(job => job.id_job === this.props.match.params.id)[0].percentage }
                  text={`${this.props.jobs.filter(job => job.id_job === this.props.match.params.id)[0].percentage}%`}
                />
            </div>
          </div>
          <div className="graph-container">
            <p className = "dashboard-info-block-heading">DETAIL</p>
            <p>Computer users and programmers have become so accustomed to using Windows, even for the changing capabilities and the appearances of the graphical interface of the versions, therefore it has remained Microsoft’s product. Although, Lycoris, Red Hat, Mandrake, Suse, Knoppix, Slackware and Lindows make up some of the different versions of LINUX. </p>
          </div>
        </div>
        <div className="main-logs-container">
            <div className = "log-headers">
              <h4>LOG</h4> <button>error<span>0</span></button> <button>warning<span>0</span></button> <button>info<span>{12* this.props.jobs.filter(job => job.id_job === this.props.match.params.id)[0].percentage}</span></button>
            </div>
            <textarea rows = "30" disabled className = "text-log">
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/

            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/
            </textarea>
            
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => {
  return {
    jobs: state.jobs,
    newJob: state.newJob
  };
};

export default connect(mapStateToProps)(FateDashboard);

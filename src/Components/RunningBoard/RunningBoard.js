import React, { Component } from "react";
import "./RunningBoard.css";
import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { Link } from "react-router-dom";
import ReactApexChart from "react-apexcharts";
import { setData, setNewJob } from "../../actions/actions";
import { connect } from "react-redux";
import axios from "axios";

class RunningBoard extends Component {
  state = {
    options: {
      chart: {
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "straight"
      },
      title: {
        text: "Product Trends by Month",
        align: "left"
      },
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      xaxis: {
        categories: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep"
        ]
      }
    },
    series: [
      {
        name: "Desktops",
        data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
      }
    ],
    newJob: {}
  };

  componentDidMount = () => {
    window.scrollTo(0, 0);

    let intervalId1 = setInterval(
      function() {
        let jobsSession = JSON.parse(sessionStorage.getItem("jobs"));
        if (jobsSession[0].percentage === 100) {
          clearInterval(intervalId1);
        }

        if (jobsSession[0].percentage !== 100) {
          let jobs = jobsSession;
          let newJob = jobs.map(job => {
            if (job.id_job === "100000000000000000000001") {
              return {
                ...job,
                percentage: job.percentage + 1
              };
            }

            return job;
          });

          sessionStorage.setItem("jobs", JSON.stringify(newJob));
          this.props.setData();
        }
      }.bind(this),
      8000
    );

    let intervalId2 = setInterval(
      function() {
        let jobsSession = JSON.parse(sessionStorage.getItem("jobs"));
        if (jobsSession[1].percentage === 100) {
          clearInterval(intervalId2);
        }

        if (jobsSession[1].percentage !== 100) {
          let jobs = jobsSession;
          let newJob = jobs.map(job => {
            if (job.id_job === "100000000000000000000002") {
              return {
                ...job,
                percentage: job.percentage + 1
              };
            }

            return job;
          });

          sessionStorage.setItem("jobs", JSON.stringify(newJob));
          this.props.setData();
        }
      }.bind(this),
      10000
    );

    let intervalId4 = setInterval(
      function() {
        let jobsSession = JSON.parse(sessionStorage.getItem("jobs"));
        if (jobsSession[2].percentage === 100) {
          clearInterval(intervalId4);
        }

        if (jobsSession[1].percentage !== 100) {
          let jobs = jobsSession;
          let newJob = jobs.map(job => {
            if (job.id_job === "100000000000000000000003") {
              return {
                ...job,
                percentage: job.percentage + 1
              };
            }

            return job;
          });

          sessionStorage.setItem("jobs", JSON.stringify(newJob));
          this.props.setData();
        }
      }.bind(this),
      15000
    );

    let intervalId5 = setInterval(
      function() {
        let jobsSession = JSON.parse(sessionStorage.getItem("jobs"));
        if (jobsSession[3].percentage === 100) {
          clearInterval(intervalId5);
        }

        if (jobsSession[1].percentage !== 100) {
          let jobs = jobsSession;
          let newJob = jobs.map(job => {
            if (job.id_job === "100000000000000000000004") {
              return {
                ...job,
                percentage: job.percentage + 1
              };
            }

            return job;
          });

          sessionStorage.setItem("jobs", JSON.stringify(newJob));
          this.props.setData();
        }
      }.bind(this),
      20000
    );

    let intervalId3 = setInterval(
      function() {
        let id = this.props.newJob.id_job;
        console.log(id);
        if (id === "100000000000000000000006") {
          this.waitForRunningJob();
          clearInterval(intervalId3);
        }

        const config = { headers: { "Content-Type": "multipart/form-data" } };

        axios
          .get("http://35.200.253.15/createjob/", {}, config)
          .then(data => {
            if (data.data.jobs.id_job !== "0") {
              console.log(data.data.jobs);
              sessionStorage.setItem("newJob", JSON.stringify(data.data.jobs));
              this.props.setNewJob();
            }
          })
          .catch(error => {
            alert("error.response.data.Response");
            console.log(error);
          });
      }.bind(this),
      3000
    );
  };

  waitForRunningJob = () => {
    console.log(this.props.newJob.percentage);
    let intervalId4 = setInterval(
      function() {
        if (this.props.newJob.percentage === 100) {
          clearInterval(intervalId4);
        }

        const config = { headers: { "Content-Type": "multipart/form-data" } };

        axios
          .get("http://35.200.253.15/createjob/", {}, config)
          .then(data => {
            sessionStorage.setItem("newJob", JSON.stringify(data.data.jobs));
            this.props.setNewJob();
          })
          .catch(error => {
            alert("error.response.data.Response");
            console.log(error);
          });
      }.bind(this),
      3000
    );
  };

  render() {
    return (
      <div className="running-main-container">
        <div className="running-board-description-container">
          <div className="box-container">
            <h1>Something Heading</h1>
            <p>
              Machine learning (ML) models is the most commonly used in a data
              science project. In this post, you will learn about different
              definitions of a machine learning model to get a better
              understanding of what are machine learning models
            </p>
          </div>
        </div>
        <div className="running-container">
          {this.props.jobs.map(job => {
            return (
              <div className="job-container" key={job.id_job}>
                <div className="job-detail">
                  <p className="job-id">{job.id_job}</p>
                  <p className="job-role">Role: {job.role}</p>
                </div>
                <div className="progress-bar">
                  <CircularProgressbar
                    value={job.percentage}
                    text={`${job.percentage}%`}
                  />
                </div>
                <Link
                  to={`/fate/dashboard/${job.id_job}`}
                  className="job-enter-button"
                >
                  Enter >>
                </Link>
              </div>
            );
          })}
          <div
            className={
              this.props.newJob.id_job === "100000000000000000000006"
                ? "job-container"
                : "invisible"
            }
            key={this.props.newJob.id_job}
          >
            <div className="job-detail">
              <p className="job-id">{this.props.newJob.id_job}</p>
              <p className="job-role">Role: {this.props.newJob.role}</p>
            </div>
            <div className="progress-bar">
              <CircularProgressbar
                value={this.props.newJob.percentage}
                text={`${this.props.newJob.percentage}%`}
              />
            </div>
            <Link
              to={`/fate/dashboards/${this.props.newJob.id_job}`}
              className="job-enter-button"
            >
              Enter >>
            </Link>
          </div>
        </div>
        <div className="centre-model-logs">
          <h1>CENTRE MODEL LOGS</h1>
          <div id="chart">
            <ReactApexChart
              options={this.state.options}
              series={this.state.series}
              type="line"
              height="350"
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    jobs: state.jobs,
    newJob: state.newJob
  };
};

export default connect(mapStateToProps, { setData, setNewJob })(RunningBoard);

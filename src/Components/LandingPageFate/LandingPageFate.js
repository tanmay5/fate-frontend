import React, { Component } from "react";
import "./LandingPageFate.css";
import { Link } from "react-router-dom";
import fluidLogo from "../../assets/group-20@2x.png";
import axios from 'axios';
import { connect } from "react-redux";
import {getData} from '../../actions/actions'

class LandingPageFate extends Component {
  getJobData = () => {
    this.props.getData()

    const config = { headers: { "Content-Type": "multipart/form-data" } };

        axios
          .delete("http://35.200.253.15/createjob/", {}, config)
          .then(data => {
            console.log("success D+")
          })
          .catch(error => {
            alert(error.response.data.Response);
            console.log(error);
          });
  };

  render() {
    return (
      <div className="demo-landing-page">
        <header className="demo-page-header">
          <img className="fluid-logo" src={fluidLogo} alt="fluid-logo" />
          {/* <div className="header-right">
            Guest
            <i className="fa fa-caret-down"></i>
          </div> */}
        </header>
        <div className="demo-page-container">
          <h1>Welcome to the Fluid demo</h1>
          <Link
            onClick={this.getJobData}
            to="/fate/projects"
            id="demo-link-1"
            className="request-demo-button pulse demo-link-width"
          >
            Enter
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    jobs: state.jobs
  };
};

export default connect(mapStateToProps, {getData})(LandingPageFate);
import fluidLogo from "../../assets/group-20@2x.png";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import './Projects.css'

export default class Projects extends Component {
  render() {
    return (
      <div className="projects-container">
        <div className="top-space" />
        <div className="demo-landing-body">
          {/* <div className="welcome-card">
            <i
              class="fa fa-caret-left"
              style={{ "font-size": "100px", color: "black" }}
            ></i>
            <h1>Welcome to the Fluid demo</h1>
            <i
              class="fa fa-caret-right"
              style={{ "font-size": "100px", color: "black" }}
            ></i>
          </div> */}
          <div className="demo-projects-body">
            <div className="porject-button-container">
              <p>Projects</p>
              <button>
                <span>+</span> Create new project
              </button>
            </div>
            <div className="project-list">
              <img src={fluidLogo} alt="cat" />
              <div className="project-detail">
                <Link to="/fate/running" className="title">
                  Project for demo
                </Link>
                <p className="detail">Credit Card Fraud Detection.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

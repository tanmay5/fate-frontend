import React from 'react';
import './App.css';
import { BrowserRouter, Route } from "react-router-dom";
import Fateboard from './Components/Fateboard/Fateboard';
import LandingPageFate from './Components/LandingPageFate/LandingPageFate'

function App() {
  return (
    <BrowserRouter >
      <Route path = "/" component = {LandingPageFate} exact/>
      <Route path = "/fate" component = {Fateboard} />
    </BrowserRouter>
  );
}

export default App;

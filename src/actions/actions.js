import axios from "axios";

export const getData = () => dispatch => {
    const config = { headers: { "Content-Type": "multipart/form-data" } };
    
    axios
      .get("http://35.200.253.15/jobs/", config)
      .then(data =>
        // console.log(data.data.jobs)
        dispatch({
          type: "JOB_ENTRY",
          payload: data.data.jobs
        })
      )
      .catch(error => {
        alert(error.response.data.Response);
        console.log(error);
      });
  };


export const setData = () => dispatch => {
    dispatch({
        type: "JOB_UPDATE",
        payload: true
      })
}

export const setNewJob = () => dispatch => {
  dispatch({
    type: "NEW_JOB",
    payload: true
  })
}
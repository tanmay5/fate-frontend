
const addState = {
    jobs: [],
    newJob: {id_job: 0}
}

const reducer = (state= addState, action) => {

    if(action.type === "JOB_ENTRY") {
        let jobs = JSON.stringify(action.payload)
        sessionStorage.setItem("jobs", jobs)
        console.log(jobs)
        return {
            ...state,
            jobs: action.payload
        }
    }

    if(action.type === "JOB_UPDATE") {
        let job = JSON.parse(sessionStorage.getItem("jobs"))
        return {
            ...state,
            jobs: job,
        }
    }

    if(action.type === "NEW_JOB") {
        let newJob = JSON.parse(sessionStorage.getItem("newJob"))
        return {
            ...state,
            newJob: newJob
        }
    }

    return state
}

export default reducer;